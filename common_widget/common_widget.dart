import 'package:driver_flutter/common/colours.dart';
import 'package:driver_flutter/page/mine/my_car/auth_car/widgets/car_head_number_view.dart';
import 'package:driver_flutter/page/mine/my_car/auth_car/widgets/vehicle_type_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_clx_base/common/colours.dart';
import 'package:flutter_clx_base/flutter_clx_base.dart';

/// 卡片Container
baseCardContainer({
  required List<Widget> content,
  double minHeight = 0,
  EdgeInsetsGeometry? padding,
  EdgeInsetsGeometry? margin = const EdgeInsets.symmetric(horizontal: 10.0),
  CrossAxisAlignment crossAxisAlignment = CrossAxisAlignment.start,
  double? width,
}) {
  return Container(
    margin: margin,
    padding: padding,
    width: width,
    decoration: BoxDecoration(
      color: white,
      borderRadius: BorderRadius.circular(5.0),
    ),
    constraints: BoxConstraints(minHeight: minHeight),
    child: Column(
      crossAxisAlignment: crossAxisAlignment,
      children: content,
    ),
  );
}

/// 货源item文本标签
goodsLabelWidget({String? text, Color? textColor, Color? bgColor}) {
  return Container(
    constraints: BoxConstraints(minWidth: 75.0, minHeight: 20.0),
    decoration: BoxDecoration(
      color: bgColor,
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(10.0),
        bottomRight: Radius.circular(10.0),
      ),
    ),
    alignment: Alignment.center,
    child: Text(
      text ?? "",
      style: TextStyle(fontSize: 10.0, color: textColor),
    ),
  );
}

/// 认证title
authTitleWidget({String? title}) {
  return Container(
    margin: EdgeInsets.only(top: 10.0),
    padding: EdgeInsets.symmetric(horizontal: 20.0),
    width: double.infinity,
    child: Text(
      title ?? "",
      style:
          TextStyle(fontSize: 16.0, color: black, fontWeight: FontWeight.bold),
    ),
  );
}

/// 认证container
typedef OnClickBack = void Function(int index);
authItemWidget({
  required String title,
  required String content,
  required String assetIconPath,
  bool isPass = false,
  String passTxt = "通过",
  Color passColor = color008000,
  String? netImagePath,
  Function? onSelectImage,
  EdgeInsetsGeometry? margin,
  bool isShowCarNum = false, /// 车头车牌号是否显示，默认不展示
  String? carNumLeftTxt,
  TextEditingController? controller,
  bool? readOnly,
  Function? onVerify, /// 车头车牌号校验
  bool isShowCarType = false, /// 车牌类型是否展示，默认不展示
  OnClickBack? onClickBack, /// 车辆类型选择回调
  int carType = -1,  /// 车辆类型，默认不选【1：非牵引车 2：牵引车】
}) {
  return Container(
    margin: margin ?? EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0),
    padding: isShowCarNum ? EdgeInsets.only(top: 15.0) : EdgeInsets.symmetric(vertical: 15.0),
    decoration: BoxDecoration(
      color: colorF3F6FA,
      borderRadius: BorderRadius.circular(4.0),
    ),
    child: Column(
      children: [
        Container(
          padding: EdgeInsets.symmetric(horizontal: 15.0),
          child: Row(
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      title,
                      style: TextStyle(
                          fontSize: 16.0, color: black, fontWeight: FontWeight.bold),
                    ),
                    vGap5,
                    Text(
                      content,
                      style: TextStyle(fontSize: 12.0, color: text666),
                    ),
                  ],
                ),
              ),
              InkWell(
                onTap: () {
                  if (isPass) {
                    ToastUtil.showToast("无需重新上传");
                    return;
                  }
                  onSelectImage?.call();
                },
                child: Container(
                  width: 177.0,
                  height: 122.0,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: ImageUtils.getAssetImage("add_id_card_bg"),
                    ),
                  ),
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      !netImagePath.isNullOrEmpty()
                          ? ImageWidget.loadNetImage(netImagePath,
                          width: 145, height: 90.0)
                          : ImageWidget.loadAssetImage(assetIconPath,
                          width: 145.0, height: 90.0),
                      Offstage(
                        offstage: !isPass,
                        child: Text(
                          passTxt,
                          style: TextStyle(
                              fontSize: 18.0,
                              color: passColor,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
        /// 车头车牌号或挂车车牌号
        Offstage(
          offstage: !isShowCarNum,
          child: Column(
            children: [
              vGap10,
              line(color: colorCEDCE7, height: 0.5),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 15.0),
                child: CarHeadNumberWidget(
                  leftTxt: carNumLeftTxt,
                  controller: controller,
                  readOnly: isPass,
                  onVerify: () {
                    onVerify?.call();
                  },
                ),
              ),
            ],
          ),
        ),
        /// 车辆类型
        Offstage(
          offstage: !(isShowCarNum && isShowCarType), /// 两个变量都要要显示时才显示
          child: VehicleTypeWidget(
            readOnly: isPass,
            type: carType,
            onClickBack: (int index) {
              onClickBack?.call(index);
            },
          ),
        ),
      ],
    ),
  );
}

/// 失败widget
failWidget({String? failContent}) {
  return Offstage(
    offstage: failContent.isNullOrEmpty(),
    child: Container(
      margin: EdgeInsets.all(10.0),
      padding: EdgeInsets.all(10.0),
      decoration: BoxDecoration(
        color: colorF25D5D,
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "审核失败",
                  style: TextStyle(fontSize: 18.0, color: white),
                ),
                vGap8,
                Text(
                  "失败原因：",
                  style: TextStyle(fontSize: 10.0, color: colorE9),
                ),
                vGap5,
                Text(
                  failContent ?? "",
                  style: TextStyle(fontSize: 12.0, color: white),
                )
              ],
            ),
          ),
          ImageWidget.loadAssetImage("ic_audit_failure",
              width: 65.0, height: 65.0),
        ],
      ),
    ),
  );
}
