import 'package:driver_flutter/api/net.dart';
import 'package:driver_flutter/common/bean/car_info_bean.dart';
import 'package:driver_flutter/common/sp_key.dart';
import 'package:driver_flutter/page/main/mine/mine_logic.dart';
import 'package:driver_flutter/page/mine/my_car/auth_car/auth_car_state.dart';
import 'package:driver_flutter/page/mine/my_car/auth_car/widgets/auth_car_select_image_dialog.dart';
import 'package:driver_flutter/page/mine/my_car/list/my_car_logic.dart';
import 'package:driver_flutter/page/mine/user_info/user_auth/user_auth_logic.dart';
import 'package:driver_flutter/page/mine/user_info/user_info_logic.dart';
import 'package:driver_flutter/utils/sp_util.dart';
import 'package:driver_flutter/utils/utils.dart';
import 'package:driver_flutter/widget/normal_dialog.dart';
import 'package:flutter_clx_base/flutter_clx_base.dart';

class AuthCarLogic extends GetxController {
  final AuthCarState state = AuthCarState();

  @override
  void onInit() {
    Map? map = Get.arguments;
    state.operateType = map?["type"] ?? 0;  // 操作类型：0 添加 1 修改 2 更新车辆信息
    state.carInfo.id = map?["carId"]; // 车辆认证信息
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
    // 修改车辆认证信息
    if (state.operateType == 1) {
      if (state.carInfo.id != null) {
        getCarInfoById();
      } else {
        getCarList();
      }
    }

    if (state.operateType == 2) {
      _getTruckVerifyInfo();
    }
  }

  /// 获取指定车辆认证信息
  getCarInfoById() {
    var params = {"truckId": state.carInfo.id};
    fetch(
      Method.get,
      url: HttpApi.getTruckAuthMessage,
      queryParameters: params,
      onSuccess: (result) {
        state.carInfo = CarInfoBean.fromJson(result);
        _reviewCarAuthInfo();
        update();
      },
    );
  }

  /// 获取车辆信息
  getCarList() {
    var params = {
      "page": 1,
      "pageSize": 1000,
      "flag": 1, // 是否过滤已经出车默认是0为过滤，1为不过滤
    };
    fetchList(
      Method.get,
      url: HttpApi.getTruckMessage,
      queryParameters: params,
      onSuccess: (List? list) {
        // 获取第一条审核失败的车辆
        state.carInfo = CarInfoBean.fromJson(
            list?.firstWhereOrNull((element) => element["status"] == 1));
        update();
      },
    );
  }

  /// 获取验车信息
  _getTruckVerifyInfo() {
    var params = {"id": state.carInfo.id};
    fetch(
      Method.get,
      url: HttpApi.getTruckVerifyInfo,
      queryParameters: params,
      onSuccess: (result) {
        state.carInfo = CarInfoBean.fromJson(result);
        _editCarAuthInfo();
        update();
      },
    );
  }

  /// 回显车辆认证信息
  _reviewCarAuthInfo() {
    if (!state.carInfo.licenceImgMain2.isNullOrEmpty() ||
        !state.carInfo.licenceImgSecond2.isNullOrEmpty() ||
        !state.carInfo.licenceImgSecond2Back.isNullOrEmpty() ||
        !state.carInfo.transportLicenceImg2.isNullOrEmpty()) {
      state.carType = 2; // 有挂车信息
      state.mCarType = 2;
    } else {
      state.carType = 1; // 无挂车信息
      state.mCarType = 1;
    }
    // 修改车辆认证
    if (state.operateType == 1) {
      state.licenceImgMainPass = state.carInfo.licenceImgMainOk != 1 &&
          !state.carInfo.licenceImgMain.isNullOrEmpty();
      if (!state.licenceImgMainPass) {
        state.carInfo.licenceImgMain = "";
      }
      //车辆上传优化需求
      //若车头主页照片为不可编辑状态，则文本框不可编辑，文本框内容取值自后台审核的车头车牌号。
      if (state.licenceImgMainPass) {
        state.mainCarTextEditingController.text = state.carInfo.truckNo ?? '';
      }
      state.licenceImgSecondPass = state.carInfo.licenceImgSecondOk != 1 &&
          !state.carInfo.licenceImgSecond.isNullOrEmpty();
      if (!state.licenceImgSecondPass) {
        state.carInfo.licenceImgSecond = "";
      }
      state.licenceImgSecondBackPass =
          state.carInfo.licenceImgSecondBackOk != 1 &&
              !state.carInfo.licenceImgSecondBack.isNullOrEmpty();
      if (!state.licenceImgSecondBackPass) {
        state.carInfo.licenceImgSecondBack = "";
      }
      state.transportLicenceImgPass =
          state.carInfo.transportLicenceImgOk != 1 &&
              !state.carInfo.transportLicenceImg.isNullOrEmpty();
      if (!state.transportLicenceImgPass) {
        state.carInfo.transportLicenceImg = "";
      }

      //挂车主页
      state.licenceImgMain2Pass = state.carInfo.licenceImgMain2Ok != 1 &&
          !state.carInfo.licenceImgMain2.isNullOrEmpty();
      if (!state.licenceImgMain2Pass) {
        state.carInfo.licenceImgMain2 = "";
      }
      //车辆上传优化需求
      //若挂车主页照片为不可编辑状态，则文本框不可编辑，文本框内容取值自后台审核的挂车车牌号。
      if (state.licenceImgMain2Pass) {
        state.trailerCarTextEditingController.text = state.carInfo.truckNo2 ?? '';
      }

      //挂车副页
      state.licenceImgSecond2Pass = state.carInfo.licenceImgSecond2Ok != 1 &&
          !state.carInfo.licenceImgSecond2.isNullOrEmpty();
      if (!state.licenceImgSecond2Pass) {
        state.carInfo.licenceImgSecond2 = "";
      }

      //挂车副页背面
      state.licenceImgSecond2PassBack =
          state.carInfo.licenceImgSecond2BackOk != 1 &&
              !state.carInfo.licenceImgSecond2Back.isNullOrEmpty();
      if (!state.licenceImgSecond2PassBack) {
        state.carInfo.licenceImgSecond2Back = "";
      }

      //挂车道路运输许可证
      state.transportLicenceImg2Pass =
          state.carInfo.transportLicenceImg2Ok != 1 &&
              !state.carInfo.transportLicenceImg2.isNullOrEmpty();
      if (!state.transportLicenceImg2Pass) {
        state.carInfo.transportLicenceImg2 = "";
      }
    }
  }

  /// 回显 更新车辆信息
  _editCarAuthInfo() {
    // 车辆类型：1非挂车 2挂车
    state.carType = state.carInfo.trailerType == 1 ? 2 : 1;
    state.mCarType = state.carInfo.trailerType == 1 ? 2 : 1;

    /// 需求：若仅车头行驶证过期，则挂车证件（行驶证和资格证）回写无需重新上传；
    /// 若仅挂车行驶证过期，车头证件（行驶证和资格证）回写无需重新上传；
    /// 车头：1、车头主页（行驶证主页）；2、车头副页（行驶证副页）；3、第二页（行驶证第二页）4、车头道路运输证；
    /// 挂车：5、挂车主页（挂车行驶证主页）；6、挂车副页（行驶证副页）；7、挂车运输证（挂车道路运输证）
    /// 3-车头及挂车需要验车
    if (state.carInfo.truckVerifyStatus == 3) {
      // state.carInfo.licenceImgMain = "";
      // state.carInfo.transportLicenceImg = "";
      state.carInfo.licenceImgSecond = "";
      state.carInfo.licenceImgSecondBack = "";

      /// 回写车头主页、车头车牌号
      state.licenceImgMainPass = state.carInfo.licenceImgMainOk != 1 && !state.carInfo.licenceImgMain.isNullOrEmpty();
      if (!state.licenceImgMainPass) {
        state.carInfo.licenceImgMain = "";
      }
      if (state.licenceImgMainPass) {
        state.mainCarTextEditingController.text = state.carInfo.truckNo ?? '';
      }

      /// 【2】更新验车回写道路运输证图片（主车和挂车）
      /// 车头道路运输许可证主页
      state.transportLicenceImgPass = state.carInfo.transportLicenceImgOk != 1 && !state.carInfo.transportLicenceImg.isNullOrEmpty();
      if (!state.transportLicenceImgPass) {
        state.carInfo.transportLicenceImg = "";
      }

      /// 回写挂车主页
      // state.carInfo.licenceImgMain2 = "";
      // state.carInfo.transportLicenceImg2 = "";
      state.carInfo.licenceImgSecond2 = "";
      state.carInfo.licenceImgSecond2Back = "";

      /// 回写挂车主页
      state.licenceImgMain2Pass = state.carInfo.licenceImgMain2Ok != 1 && !state.carInfo.licenceImgMain2.isNullOrEmpty();
      if (!state.licenceImgMain2Pass) {
        state.carInfo.licenceImgMain2 = "";
      }
      if (state.licenceImgMain2Pass) {
        state.trailerCarTextEditingController.text = state.carInfo.truckNo2 ?? '';
      }

      /// 回写挂车道路运输许可证
      state.transportLicenceImg2Pass = state.carInfo.transportLicenceImg2Ok != 1 && !state.carInfo.transportLicenceImg2.isNullOrEmpty();
      if (!state.transportLicenceImg2Pass) {
        state.carInfo.transportLicenceImg2 = "";
      }
    }

    /// 更新挂车，回写车头
    if (state.carInfo.truckVerifyStatus == 2) {
      // state.carInfo.licenceImgMain2 = "";
      // state.carInfo.transportLicenceImg2 = "";
      state.carInfo.licenceImgSecond2 = "";
      state.carInfo.licenceImgSecond2Back = "";

      /// 回写挂车主页
      state.licenceImgMain2Pass = state.carInfo.licenceImgMain2Ok != 1 && !state.carInfo.licenceImgMain2.isNullOrEmpty();
      if (!state.licenceImgMain2Pass) {
        state.carInfo.licenceImgMain2 = "";
      }
      if (state.licenceImgMain2Pass) {
        state.trailerCarTextEditingController.text = state.carInfo.truckNo2 ?? '';
      }
      /// 回写挂车道路运输许可证
      state.transportLicenceImg2Pass = state.carInfo.transportLicenceImg2Ok != 1 && !state.carInfo.transportLicenceImg2.isNullOrEmpty();
      if (!state.transportLicenceImg2Pass) {
        state.carInfo.transportLicenceImg2 = "";
      }

      /// 回写车头主页
      state.licenceImgMainPass = state.carInfo.licenceImgMainOk != 1 && !state.carInfo.licenceImgMain.isNullOrEmpty();
      if (!state.licenceImgMainPass) {
        state.carInfo.licenceImgMain = "";
      }
      if (state.licenceImgMainPass) {
        state.mainCarTextEditingController.text = state.carInfo.truckNo ?? '';
      }
      /// 回写车头道路运输许可证
      state.transportLicenceImgPass = state.carInfo.transportLicenceImgOk != 1 && !state.carInfo.transportLicenceImg.isNullOrEmpty();
      if (!state.transportLicenceImgPass) {
        state.carInfo.transportLicenceImg = "";
      }

      /// 车头副页
      state.licenceImgSecondPass = state.carInfo.licenceFrontImgSecondOk != 1 && !state.carInfo.licenceFrontImgSecond.isNullOrEmpty();
      if (!state.licenceImgSecondPass) {
        state.carInfo.licenceImgSecond = "";
      } else {
        state.carInfo.licenceImgSecond = state.carInfo.licenceFrontImgSecond;
      }

      /// 车头第二页
      state.licenceImgSecondBackPass = state.carInfo.licenceImgSecondBackOk != 1 && !state.carInfo.licenceImgSecondBack.isNullOrEmpty();
      if (!state.licenceImgSecondBackPass) {
        state.carInfo.licenceImgSecondBack = "";
      }
    }

    /// 更新车头，回写挂车
    if (state.carInfo.truckVerifyStatus == 1) {
      // state.carInfo.licenceImgMain = "";
      // state.carInfo.transportLicenceImg = "";
      state.carInfo.licenceImgSecond = "";
      state.carInfo.licenceImgSecondBack = "";

      /// 车头行驶证主页
      state.licenceImgMainPass = state.carInfo.licenceImgMainOk != 1 && !state.carInfo.licenceImgMain.isNullOrEmpty();
      if (!state.licenceImgMainPass) {
        state.carInfo.licenceImgMain = "";
      }
      if (state.licenceImgMainPass) {
        state.mainCarTextEditingController.text = state.carInfo.truckNo ?? '';
      }
      /// 车头道路运输许可证
      state.transportLicenceImgPass = state.carInfo.transportLicenceImgOk != 1 && !state.carInfo.transportLicenceImg.isNullOrEmpty();
      if (!state.transportLicenceImgPass) {
        state.carInfo.transportLicenceImg = "";
      }

      /// 回写挂车主页
      state.licenceImgMain2Pass = state.carInfo.licenceImgMain2Ok != 1 && !state.carInfo.licenceImgMain2.isNullOrEmpty();
      if (!state.licenceImgMain2Pass) {
        state.carInfo.licenceImgMain2 = "";
      }
      if (state.licenceImgMain2Pass) {
        state.trailerCarTextEditingController.text = state.carInfo.truckNo2 ?? '';
      }
      /// 回写挂车道路运输许可证
      state.transportLicenceImg2Pass = state.carInfo.transportLicenceImg2Ok != 1 && !state.carInfo.transportLicenceImg2.isNullOrEmpty();
      if (!state.transportLicenceImg2Pass) {
        state.carInfo.transportLicenceImg2 = "";
      }

      /// 回写挂车副页
      state.licenceImgSecond2Pass = state.carInfo.licenceImgSecond2Ok != 1 && !state.carInfo.licenceImgSecond2.isNullOrEmpty();
      if (!state.licenceImgSecond2Pass) {
        state.carInfo.licenceImgSecond2 = "";
      }

      /// 回写挂车副页背面
      state.licenceImgSecond2PassBack = state.carInfo.licenceImgSecond2BackOk != 1 && !state.carInfo.licenceImgSecond2Back.isNullOrEmpty();
      if (!state.licenceImgSecond2PassBack) {
        state.carInfo.licenceImgSecond2Back = "";
      }
    }
  }

  /// 联系客服
  onCall() {
    KeyBoardUtils.hideKeyboard();
    callServerPhone();
  }

  /// 选择图片
  onSelectImage(int type) {
    KeyBoardUtils.hideKeyboard();
    String title = "";
    String iconPath = "";
    switch (type) {
      case 1:
        title = "车头行驶证主页上传示例";
        iconPath = "sample_icon_car_head_main";
        break;
      case 2:
        title = "车头行驶证副页上传示例";
        iconPath = "sample_icon_car_head_back";
        break;
      case 3:
        title = "车头行驶证第二页上传示例";
        iconPath = "sample_icon_car_head_deputy_back";
        break;
      case 4:
        title = "车头运输资格证上传示例";
        iconPath = "sample_icon_car_employed";
        break;
      case 5:
        title = "挂车行驶证主页上传示例";
        iconPath = "sample_icon_car_head_main";
        break;
      case 6:
        title = "挂车行驶证副页上传示例";
        iconPath = "sample_icon_car_trailer_back";
        break;
      case 7:
        title = "挂车运输资格证上传示例";
        iconPath = "sample_icon_car_employed";
        break;
      case 8:
        title = "挂车行驶证副页背面上传示例";
        iconPath = "sample_icon_car_head_deputy_back";
        break;
    }
    _selectImageDialog(title, iconPath, type);
  }

  /// 展示选择图片
  _showSelectImage(int type, String? url) {
    logger.d('--_showSelectImage--: type=$type, url=$url');
    switch (type) {
      case 1:
        state.carInfo.licenceImgMain = url;
        break;
      case 2:
        state.carInfo.licenceImgSecond = url;
        break;
      case 3:
        state.carInfo.licenceImgSecondBack = url;
        break;
      case 4:
        state.carInfo.transportLicenceImg = url;
        break;
      case 5:
        state.carInfo.licenceImgMain2 = url;
        break;
      case 6:
        state.carInfo.licenceImgSecond2 = url;
        break;
      case 7:
        state.carInfo.transportLicenceImg2 = url;
        break;
      case 8: //挂车行驶证副页背面（需要知道哪个字段代表）
        state.carInfo.licenceImgSecond2Back = url;
        break;
    }
    update();
  }

  /// 选择图片
  _selectImageDialog(title, iconPath, int type) {
    logger.d("--_selectImageDialog--: title=$title, iconPath=$iconPath, type=$type");
    Get.bottomSheet(
      AuthCarSelectImageDialog(
        title: title,
        iconPath: iconPath,
        onCallback: (String? url) {
          _showSelectImage(type, url);
          logger.d("type -> $type state.operateType -> ${state.operateType}");
          ///车辆上传优化
          if ((type == 1 && state.licenceImgMainPass == false) ||
              (type == 5 && state.licenceImgMain2Pass == false)) {
            getTruckByOcr(url, type);
          }
        },
      ),
      isScrollControlled: true,
    );
  }

  /// 提交审核
  onSubmit() {
    if (_checkInfo()) {
      /// 校验车牌号
      if (state.mainCarTextEditingController.text.trim().length == 7 ||
          state.mainCarTextEditingController.text.trim().length == 8) {
        /// 检验车头车牌号
        onSubmitCheck(1);
      }
    }
  }

  /// 继续提交
  nextOnSubmit() {
    logger.d("--nextOnSubmit--: 继续提交");
    if (state.carType == 1) {
      // 提示是否填写挂车信息
      _completeTrailerDialog();
      return;
    }
    _submitAuthInfo();
  }

  /// 信息校验
  _checkInfo() {
    if (state.carInfo.licenceImgMain.isNullOrEmpty()) {
      ToastUtil.showToast("请选择车头行驶证主页图片");
      return false;
    }
    /// 校验车头车牌号
    if (!(state.mainCarTextEditingController.text.trim().length == 7 ||
        state.mainCarTextEditingController.text.trim().length == 8)) {
      ToastUtil.showToast("请输入正确的车头车牌号");
      return false;
    }
    if (state.mCarType == -1) {
      ToastUtil.showToast("请选择车辆类型");
      return false;
    }
    if (state.carInfo.licenceImgSecond.isNullOrEmpty()) {
      ToastUtil.showToast("请选择车头行驶证副页图片");
      return false;
    }
    if (state.carInfo.licenceImgSecondBack.isNullOrEmpty()) {
      ToastUtil.showToast("请选择车头行驶证副页背面图片");
      return false;
    }
    if (state.carInfo.transportLicenceImg.isNullOrEmpty()) {
      ToastUtil.showToast("请选择车头道路运输许可证主页图片");
      return false;
    }
    if (state.carType == 2 && state.carInfo.licenceImgMain2.isNullOrEmpty()) {
      ToastUtil.showToast("请选择挂车行驶证主页图片");
      return false;
    }
    /// 校验挂车车牌号
    if (state.carType == 2 &&
        !(state.trailerCarTextEditingController.text.trim().length == 7 ||
            state.trailerCarTextEditingController.text.trim().length == 8)) {
      ToastUtil.showToast("请输入正确的挂车车牌号");
      return false;
    }
    if (state.carType == 2 && state.carInfo.licenceImgSecond2.isNullOrEmpty()) {
      ToastUtil.showToast("请选择挂车行驶证副页图片");
      return false;
    }
    //挂车行驶证副页背面
    if (state.carType == 2 && state.carInfo.licenceImgSecond2Back.isNullOrEmpty()) {
      ToastUtil.showToast("请选择挂车行驶证副页背面图片");
      return false;
    }
    if (state.carType == 2 &&
        state.carInfo.transportLicenceImg2.isNullOrEmpty()) {
      ToastUtil.showToast("请选择挂车道路运输许可证主页图片");
      return false;
    }
    return true;
  }

  /// 完善挂车信息
  _completeTrailerDialog() {
    Get.dialog(NormalDialog(
      title: "温馨提示",
      content: "是否完善挂车信息",
      cancelBtnText: "暂无挂车",
      confirmBtnText: "完善挂车信息",
      onCancel: () {
        state.carType = 1;
        state.mCarType = 1;
        _submitAuthInfo();
      },
      onConfirm: () {
        state.carType = 2;
        state.mCarType = 2;
        update();
      },
    ));
  }

  /// 提交审核
  _submitAuthInfo() {
    state.carInfo.userId = getMyUserID();
    state.carInfo.type = state.carType;  /// 车辆类型传参
    String txt1 = state.mainCarTextEditingController.text.trim();
    String txt2 = state.trailerCarTextEditingController.text.trim();
    state.carInfo.truckNo = txt1; //车头车牌号
    state.carInfo.truckNo2 = txt2; //挂车车牌号
    if (state.carInfo.id != null) {
      // 修改车辆
      state.carInfo.truckId = state.carInfo.id;
      // 华潞通需求
      _saveOrUpdateTruck();
    } else {
      //华潞通需求
      _saveOrUpdateTruck();
    }
  }

  /// 车辆认证需求 编辑车辆认证
  _saveOrUpdateTruck() {
    fetch(
      Method.post,
      url: HttpApi.saveOrUpdateTruck,
      params: state.carInfo.toJson(),
      onSuccess: (result) {
        if (getAuthRole() == 0) {
          _authSuccess(); // 已经是车主角色了
        } else {
          _saveUserAuthType(); // 不是车主，认证车主身份
        }
      },
    );
  }

  /// 认证车主身份
  _saveUserAuthType() {
    var params = {"userAuthType": 0};
    fetch(
      Method.post,
      url: HttpApi.saveUserAuthType,
      queryParameters: params,
      onSuccess: (result) {
        SpUtil.putInt(authRole, 0);
        _authSuccess();
      },
    );
  }

  /// 认证成功
  _authSuccess() {
    ToastUtil.showToast("提交成功！");
    findOtherLogic<MyCarLogic>()?.state.listController.callRefresh();
    findOtherLogic<UserInfoLogic>()?.getAuthStatus(); // 刷新个人信息
    findOtherLogic<UserAuthLogic>()?.getAuthStatus(); // 刷新车主、司机认证中心
    findOtherLogic<MineLogic>()?.getAuthStatus(); // 刷新个人中心认证状态
    Get.back();
  }

  /// 退出
  onBack() {
    KeyBoardUtils.hideKeyboard();
    Get.dialog(NormalDialog(
      title: "认证提醒",
      content: "内容还未提交，确认离开？",
      confirmBtnText: "离开",
      onConfirm: () {
        Get.back();
      },
    ));
    return Future.value(false);
  }

  onBackTap() {
    KeyBoardUtils.hideKeyboard();
  }

  /// 车辆上传优化
  /// 车头车牌号校验 1: 车头主页 2：挂车主页
  onVerify(int type) {
    if (type == 1 && state.mainCarTextEditingController.text.trim().isEmpty) { return; }
    if (type == 2 && state.trailerCarTextEditingController.text.trim().isEmpty) { return; }
    var params = {
      "truckNo": type == 1
          ? state.mainCarTextEditingController.text.trim()
          : state.trailerCarTextEditingController.text.trim(),
      "type": type,
      "userId": getMyUserID(),
      "truckId": state.carInfo.id, /// 4月15日新增
    };
    fetch(
      Method.post,
      url: HttpApi.getTruckNoExist,
      params: params,
      onSuccess: (result) {
        logger.d("--onVerify--: result=$result");
        if (result != null && result['msg'] != null) { //不合格
          //只返回提示内容，便于扩展，不用升级
          onDialogEvent(result['msg']);
        }
        update();
      },
    );
  }

  /// 车头车牌号ocr识别
  /// v6.9.23：新增字段：trailerType（车辆类型：0非牵引车 1牵引车）
  void getTruckByOcr(String? url, int type) {
    var params = {"img": '/$url'};
    fetch(
      Method.get,
      url: HttpApi.getTruckByOcr,
      queryParameters: params,
      onSuccess: (result) {
        logger.d("--getTruckByOcr--: result=$result");
        if (result != null) {
          if (type == 1) {
            state.carInfo.truckNo = result['truckNo'];
            state.mainCarTextEditingController.text = result['truckNo'];
            /// 自动选中牵引车、非牵引车
            if (result['trailerType'] != null) {
              state.carInfo.trailerType = result['trailerType'];
              state.carType = state.carInfo.trailerType == 1 ? 2 : 1;
              state.mCarType = state.carInfo.trailerType == 1 ? 2 : 1;
            }
          }
          if (type == 5) {
            state.carInfo.truckNo2 = result['truckNo'];
            state.trailerCarTextEditingController.text = result['truckNo'];
          }
          update();
        } else { ///清空输入框
          if (type == 1) {
            state.carInfo.truckNo = '';
            state.mainCarTextEditingController.text = '';
            state.mCarType = -1;
          }
          if (type == 5) {
            state.carInfo.truckNo2 = '';
            state.trailerCarTextEditingController.text = '';
          }
          update();
        }
      },
    );
  }

  /// - 该司机审核中的车辆中是否有此车头若存在，则弹窗提示用户
  /// - 该车头车牌号是否在数据库中已存在（同车辆入驻车牌号已存在），若存在则提示用户
  /// 车主姓名和手机号脱敏处理，例如:胡*康，胡*，133****2222
  onDialogEvent(String msg) {
    Get.dialog(NormalDialog(
        title: "温馨提示",
        content: msg,
        confirmBtnText: "确定",
        cancelBtnText: null,
        onConfirm: () {}));
  }

  /// 提交时：车头车牌号校验 1: 车头主页 2：挂车主页
  onSubmitCheck(int type) {
    var params = {
      "truckNo": type == 1
          ? state.mainCarTextEditingController.text.trim()
          : state.trailerCarTextEditingController.text.trim(),
      "type": type,
      "userId": getMyUserID(),
      "truckId": state.carInfo.id, /// 4月15日新增
    };
    fetch(
      Method.post,
      url: HttpApi.getTruckNoExist,
      params: params,
      onSuccess: (result) {
        logger.d("--onSubmitCheck--: result=$result");
        if (result != null && result['msg'] != null) { //不合格
          //只返回提示内容，便于扩展，不用升级
          onDialogEvent(result['msg']);
        } else {
          if (type == 1) {
            _checkTrailer();
          }
          if (type == 2) {
            nextOnSubmit(); ///继续提交
          }
        }
        update();
      },
    );
  }

  /// 挂车车牌号是否需要检验
  _checkTrailer() {
    if (state.carType == 2 &&
        (state.trailerCarTextEditingController.text.trim().length == 7 ||
            state.trailerCarTextEditingController.text.trim().length == 8)) {
      /// 检验挂车车牌号
      onSubmitCheck(2);
    } else { /// 不需要检验, 继续提交
      nextOnSubmit();
    }
  }

  /// 车辆类型 【1：非牵引车 2：牵引车】
  onClickBack(int type) {
    logger.d('--onClickBack--: type=$type');
    state.carType = type;
    state.mCarType = type;
    update();
  }
}
