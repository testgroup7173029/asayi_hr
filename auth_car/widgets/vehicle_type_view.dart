import 'package:driver_flutter/common/colours.dart';
import 'package:driver_flutter/page/mine/my_car/auth_car/widgets/car_type_item_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_clx_base/flutter_clx_base.dart';

typedef OnClickBack = void Function(int index);
/// 车辆类型的 widget
/// driver —v6.9.23   OCR识别优化
/// 业务背景：配合场站业务，支持用户设置收支车辆自卸类型与方式。
class VehicleTypeWidget extends StatefulWidget {
  final bool readOnly;   /// 是不是只读
  final int type;        /// 默认选中的哪一个【1：非牵引车 2：牵引车】
  final OnClickBack? onClickBack;  /// 选择点击的回调

  const VehicleTypeWidget({
    super.key,
    this.readOnly = false,
    this.type = -1,
    this.onClickBack,
  });

  @override
  State<StatefulWidget> createState() {
    return _VehicleTypeWidgetState();
  }
}

class _VehicleTypeWidgetState extends State<VehicleTypeWidget> {
  int _mType = -1;

  @override
  void initState() {
    _mType = widget.type;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 15.0, right: 15.0, bottom: 15.0),
      child: Row(
        children: [
          Text(
            '车辆类型  ',
            style: TextStyle(
              fontSize: 14.0,
              fontWeight: FontWeight.bold,
              color: color000000,
            ),
          ),
          hGap10,
          Expanded(
            child: GridView.builder(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemCount: 2,
              padding: EdgeInsets.zero,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                childAspectRatio: 1 / 0.23,
              ),
              itemBuilder: (context, index) {
                return getItem(context, index);
              },
            ),
          ),
        ],
      ),
    );
  }

  getItem(context, index) {
    return GestureDetector(
      onTap: () {
        KeyBoardUtils.hideKeyboard();
        _onTap(index);
      },
      child: CarTypeItemWidget(
        content: index == 0 ? '牵引车' : '非牵引车',
        isShow: _getCurrentShow(index),
      ),
    );
  }

  /// 处理当前点击事件
  _onTap(index) {
    if (widget.readOnly) { return; }
    logger.d('--_onTap--: index=$index');

    switch(index) {
      case 0: _mType = 2; break; /// 牵引车
      case 1: _mType = 1; break; /// 非牵引车
    }
    setState(() { });

    /// 点击事件回调【1：非牵引车 2：牵引车】
    widget.onClickBack?.call(_mType);
  }

  /// 获取当前是否显示 【1：非牵引车 2：牵引车】
  _getCurrentShow(index) {
    // logger.d('---_getCurrentShow---: type=${widget.type}');
    if (widget.type == index) { /// 1: 非挂车
      return true;
    }
    if (widget.type == index+2) { /// 2: 挂车
      return true;
    }
    return false;
  }
}