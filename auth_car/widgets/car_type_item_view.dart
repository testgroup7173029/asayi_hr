import 'package:driver_flutter/common/colours.dart';
import 'package:flutter/material.dart';
import 'package:flutter_clx_base/flutter_clx_base.dart';

/// 车辆类型 item widget
/// driver —v6.9.23   OCR识别优化
/// 业务背景：配合场站业务，支持用户设置收支车辆自卸类型与方式。
class CarTypeItemWidget extends StatefulWidget {
  final String content;
  final bool isShow;

  const CarTypeItemWidget({
    super.key,
    this.content = '',
    this.isShow = false,
  });

  @override
  State<StatefulWidget> createState() {
    return CarTypeItemWidgetState();
  }
}

class CarTypeItemWidgetState extends State<CarTypeItemWidget> {
  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        hGap5,
        Container(
          width: 25.0,
          height: 25.0,
          color: Colors.transparent,
          child: Center(
            child: Container(
              width: 13.0,
              height: 13.0,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20.0),
                border: Border.all(color: grayCB, width: 1.5),
                color: widget.isShow == true ? Color(0xFFEa5529) : null,
              ),
            ),
          ),
        ),
        Text(
          widget.content,
          style: TextStyle(
            fontSize: 12.0,
            fontWeight: FontWeight.normal,
            color: color000000,
          ),
          textAlign: TextAlign.center,
          strutStyle: StrutStyle(
            fontSize: 12.0,
            forceStrutHeight: true,
          ),
        ),
      ],
    );
  }
}