import 'package:driver_flutter/common/colours.dart';
import 'package:driver_flutter/widget/keyboard/license_keyboard.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_clx_base/flutter_clx_base.dart';

///车头主页照片为空时不显示输入框，
///车头主页照片不为空时显示输入框，
///若车头主页照片为可编辑状态，则文本框可编辑，OCR识别车头主页照片中车牌号后自动填写至文本框，支持编辑。
///若车头主页照片为不可编辑状态，则文本框不可编辑，文本框内容取值自后台审核的车头车牌号。
///车头车牌号
class CarHeadNumberWidget extends StatefulWidget {
  final String? leftTxt;
  final TextEditingController? controller;
  final bool? readOnly;
  final Function? onVerify;

  const CarHeadNumberWidget({
    super.key,
    required this.leftTxt,
    required this.controller,
    this.readOnly = false,
    this.onVerify,
  });

  @override
  State<StatefulWidget> createState() {
    return CarHeadNumberWidgetState();
  }
}

class CarHeadNumberWidgetState extends State<CarHeadNumberWidget> {
  final FocusNode _nodeText = FocusNode();
  bool _isHaveFocus = false;

  @override
  void initState() {
    //焦点监听
    _nodeText.addListener(() {
      if (widget.readOnly == true) return;  //只读情景
      if (_nodeText.hasFocus) {
        logger.d('得到焦点');
        setState(() {
          _isHaveFocus = true;
        });
      } else {
        logger.d('失去焦点: txt=${ widget.controller?.value }');
        if (widget.controller?.text.trim().length == 7 ||
            widget.controller?.text.trim().length == 8) {
          widget.onVerify?.call(); //校验
        }
        setState(() {
          _isHaveFocus = false;
        });
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(
          widget.leftTxt ?? '',
          style: TextStyle(
            fontSize: 14.0,
            fontWeight: FontWeight.bold,
            color: color000000,
          ),
        ),
        hGap10,
        Expanded(
          child: Container(
            margin: EdgeInsets.symmetric(vertical: 10.0),
            decoration: BoxDecoration(
              color: colorE9EEF7,
              borderRadius: BorderRadius.all(Radius.circular(4.0)),
              border: _isHaveFocus
                  ? Border.all(color: colorEA5529, width: 0.5)
                  : Border.all(color: Colors.transparent, width: 0.5),
            ),
            height: 32.0,
            child: GFTextField(
              readOnly: widget.readOnly,
              decoration: InputDecoration(
                contentPadding: EdgeInsets.only(top: 0.0, bottom: 0.0, left: 3.0),
                hintText: "请输入车牌号",
                hintStyle: TextStyle(fontSize: 12, color: Colors.grey),
                border: _inputBorder(),
                enabledBorder: _inputBorder(),
                focusedErrorBorder: _inputBorder(),
                errorBorder: _inputBorder(),
                disabledBorder: _inputBorder(),
                focusedBorder: _inputBorder(),
              ),
              style: TextStyle(fontSize: 14, color: color344254),
              maxLines: 1,
              controller: widget.controller,
              textInputAction: TextInputAction.done,
              focusNode: _nodeText,
              inputFormatters: [
                // FilteringTextInputFormatter.allow(RegExp("[a-zA-Z]|[0-9]")),
                LengthLimitingTextInputFormatter(8),
              ],
              keyboardType: LicenseKeyboard.inputType,
              onChanged: (v) {
                widget.controller?.value = widget.controller!.value.copyWith(text: v.toUpperCase());
              },
            ),
          ),
        ),
      ],
    );
  }
}

InputBorder _inputBorder() {
  return OutlineInputBorder(
    borderSide: BorderSide(
      width: 0,
      color: Colors.transparent,
    ),
  );
}