import 'dart:io';
import 'package:driver_flutter/api/net.dart';
import 'package:driver_flutter/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_clx_base/common/colours.dart';
import 'package:flutter_clx_base/flutter_clx_base.dart';
import 'package:flutter_clx_base/utils/image_picker_utils.dart';

/// 专门用于车辆认证，选择拍照和相册时，
/// 添加 Loading，不影响其他地方UI
class AuthCarSelectImageDialog extends StatelessWidget {
  final String? title;
  final String? iconPath;
  final Function onCallback;

  const AuthCarSelectImageDialog({
    Key? key,
    this.title,
    this.iconPath,
    required this.onCallback,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        !title.isNullOrEmpty() && !iconPath.isNullOrEmpty()
            ? cardContainer(
          children: [
            vGap20,
            Text(
              title ?? "",
              style: TextStyle(
                  fontSize: 16.0,
                  color: black,
                  fontWeight: FontWeight.bold),
            ),
            vGap20,
            ImageWidget.loadAssetImage(
              iconPath,
              height: 236.0,
            ),
            vGap20,
          ],
        )
            : SizedBox(),
        vGap10,
        cardContainer(children: [
          buttonItem(
              title: "拍照",
              onTap: () {
                Get.back();
                _selectPic(ImageSource.camera, onCallback);
              }),
          Divider(height: 1.0),
          buttonItem(
              title: "从相册选择",
              onTap: () {
                Get.back();
                _selectPic(ImageSource.gallery, onCallback);
              }),
        ]),
        vGap10,
        cardContainer(children: [
          buttonItem(
            title: "取消",
            onTap: () => Get.back(),
          )
        ]),
        vGap10,
      ],
    );
  }

  /// 圆角卡片
  cardContainer({required List<Widget> children}) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.symmetric(horizontal: 20.0),
      decoration: BoxDecoration(
        color: white,
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Column(
        children: children,
      ),
    );
  }

  /// 按钮
  buttonItem({title, onTap}) {
    return InkWell(
      onTap: onTap,
      child: Container(
        constraints: BoxConstraints(minHeight: 45.0),
        alignment: Alignment.center,
        child: Text(
          title ?? "",
          style: TextStyle(
              fontSize: 14.0, color: text21, fontWeight: FontWeight.bold),
        ),
      ),
    );
  }

  /// 选择和加载图片
  Future<void> _selectPic(ImageSource source, onSuccess) async {
    if (Platform.isIOS) {
      switch (source) {
        case ImageSource.gallery:
          _galleryIOS(source, onSuccess);
          break;
        case ImageSource.camera:
          _cameraIOS(source, onSuccess);
          break;
      }
    } else {
      _uploadImage(source, onSuccess);
    }
  }

  _galleryIOS(ImageSource source, onSuccess) async {
    if (await requestPermissionPhotos()) {
      _uploadImage(source, onSuccess);
    } else {
      if (await requestPermissionPhotosAddOnly()) {
        _uploadImage(source, onSuccess);
      } else {
        ToastUtil.showToast("无法访问相册中的照片，请前往系统设置！");
        return;
      }
    }
  }

  _cameraIOS(ImageSource source, onSuccess) async {
    if (await requestPermissionCam()) {
      _uploadImage(source, onSuccess);
    } else {
      ToastUtil.showToast("无法访问相机，请前往系统设置！");
      return;
    }
  }

  Future<void> _uploadImage(ImageSource source, onSuccess) async {
    var image = await ImagePicker().pickImage(source: source, imageQuality: 20);
    // 未选择图片时 返回值为null，不需要增加提示信息
    if (image == null) {
      return;
    }

    /// 添加Loading
    EasyLoading.instance.showLoading(text: "请稍后....");
    // 图片选择成功时，调用上传图片到服务器的方法
    String url = await uploadImage(path: image.path);
    /// 隐藏Loading
    EasyLoading.dismiss();

    onSuccess(url);
  }
}
