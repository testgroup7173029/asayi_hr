import 'package:driver_flutter/page/common_widget/common_widget.dart';
import 'package:driver_flutter/widget/button_public_radius.dart';
import 'package:flutter/material.dart';
import 'package:flutter_clx_base/common/colours.dart';
import 'package:flutter_clx_base/flutter_clx_base.dart';
import 'auth_car_logic.dart';

class AuthCarPage extends StatelessWidget {
  final logic = Get.find<AuthCarLogic>();
  final state = Get.find<AuthCarLogic>().state;

  AuthCarPage({super.key});

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => logic.onBack(),
      child: GestureDetector(
        onTap: logic.onBackTap,
        child: MyScaffold(
          appBar: MyPageAppBar(
            title: "车辆认证",
            actions: [
              IconButton(
                onPressed: logic.onCall,
                icon: ImageWidget.loadAssetImage("ic_customer_service_phone",
                    width: 25.0, height: 25.0),
              ),
            ],
          ),
          backgroundColor: white,
          body: GetBuilder<AuthCarLogic>(builder: (logic) {
            return Column(
              children: [
                vGap10,
                Expanded(
                  child: MyScrollView(
                    children: [
                      Offstage(
                        offstage: state.carInfo.status != 1,
                        child: failWidget(failContent: state.carInfo.content),
                      ),
                      authTitleWidget(title: "请上传车头行驶证照片"),
                      authItemWidget(
                        title: "车头主页",
                        content: "上传行驶证主页",
                        assetIconPath: "ic_vehicle_license_main_sample",
                        netImagePath: state.carInfo.licenceImgMain,
                        isPass: state.licenceImgMainPass,
                        onSelectImage: () => logic.onSelectImage(1),
                        carNumLeftTxt: '车头车牌号',
                        isShowCarNum: !state.carInfo.licenceImgMain.isNullOrEmpty(),
                        controller: state.mainCarTextEditingController,
                        onVerify: () => logic.onVerify(1),
                        isShowCarType: true,
                        carType: state.mCarType,
                        onClickBack: (index) => logic.onClickBack(index),
                      ),
                      authItemWidget(
                        title: "车头副页",
                        content: "上传行驶证副页",
                        assetIconPath: "ic_vehicle_license_back_sample",
                        netImagePath: state.carInfo.licenceImgSecond,
                        isPass: state.licenceImgSecondPass,
                        onSelectImage: () => logic.onSelectImage(2),
                      ),
                      authItemWidget(
                        title: "第二页",
                        content: "上传行驶证第二页",
                        assetIconPath: "ic_vehicle_license_main_sample_back",
                        netImagePath: state.carInfo.licenceImgSecondBack,
                        isPass: state.licenceImgSecondBackPass,
                        onSelectImage: () => logic.onSelectImage(3),
                      ),
                      authTitleWidget(title: "请上传车头道路运输证照片"),
                      authItemWidget(
                        title: "车头运输证",
                        content: "上传道路运输照片",
                        assetIconPath: "ic_road_license_sample",
                        netImagePath: state.carInfo.transportLicenceImg,
                        isPass: state.transportLicenceImgPass,
                        onSelectImage: () => logic.onSelectImage(4),
                      ),
                      Offstage(
                        offstage: state.carType != 2,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            authTitleWidget(title: "请上传挂车行驶证照片"),
                            authItemWidget(
                              title: "挂车主页",
                              content: "上传行驶证主页",
                              assetIconPath: "ic_vehicle_license_main_sample",
                              netImagePath: state.carInfo.licenceImgMain2,
                              isPass: state.licenceImgMain2Pass,
                              onSelectImage: () => logic.onSelectImage(5),
                              carNumLeftTxt: '挂车车牌号',
                              isShowCarNum: !state.carInfo.licenceImgMain2.isNullOrEmpty(),
                              controller: state.trailerCarTextEditingController,
                              onVerify: () => logic.onVerify(2),
                            ),
                            authItemWidget(
                              title: "挂车副页",
                              content: "上传行驶证副页",
                              assetIconPath: "ic_trailer_license_back_sample",
                              netImagePath: state.carInfo.licenceImgSecond2,
                              isPass: state.licenceImgSecond2Pass,
                              onSelectImage: () => logic.onSelectImage(6),
                            ),
                            authItemWidget(
                              title: "挂车副页背面",
                              content: "上传行驶证副页背面",
                              assetIconPath: "ic_trailer_license_back_sample_back",
                              netImagePath: state.carInfo.licenceImgSecond2Back,
                              isPass: state.licenceImgSecond2PassBack,
                              onSelectImage: () => logic.onSelectImage(8),
                            ),
                            authTitleWidget(title: "请上传挂车道路运输证照片"),
                            authItemWidget(
                              title: "挂车运输证",
                              content: "上传道路运输照片",
                              assetIconPath: "ic_road_license_sample",
                              netImagePath: state.carInfo.transportLicenceImg2,
                              isPass: state.transportLicenceImg2Pass,
                              onSelectImage: () => logic.onSelectImage(7),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                ButtonPublicRadius(
                  title: "提交审核",
                  margin: EdgeInsets.symmetric(vertical: 15.0),
                  onPress: logic.onSubmit,
                ),
              ],
            );
          }),
        ),
      ),
    );
  }
}
