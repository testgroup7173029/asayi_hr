import 'package:driver_flutter/common/bean/car_info_bean.dart';
import 'package:flutter/material.dart';

class AuthCarState {
  int carType = 1; // 车辆类型：1非挂车 2挂车
  var carInfo = CarInfoBean(); // 车辆认证信息
  int? operateType = 0; // 操作类型：0 添加 1 修改 2 更新车辆信息

  bool licenceImgMainPass = false; // 车头行驶证主页  图片合格
  bool licenceImgSecondPass = false; // 车头行驶证副页正面  图片合格
  bool licenceImgSecondBackPass = false; // 车头行驶证副页背面  图片合格
  bool transportLicenceImgPass = false; // 车头道路运输许可证主页  图片合格
  bool licenceImgMain2Pass = false; // 挂车主页  图片合格
  bool licenceImgSecond2Pass = false; // 挂车副页  图片合格
  bool licenceImgSecond2PassBack = false; // 挂车副页背面 图片合格
  bool transportLicenceImg2Pass = false; // 挂车道路运输许可证  图片合格

  var mainCarTextEditingController = TextEditingController(); //车头车牌号
  var trailerCarTextEditingController = TextEditingController(); //挂车车牌号

  /// driver —v6.9.23   OCR识别优化
  int mCarType = -1;  /// 默认不选【车辆类型：1：非牵引车 2：牵引车】

  AuthCarState() {
    carInfo.type = carType;
  }
}
