import 'package:flutter_clx_base/flutter_clx_base.dart';

import 'auth_car_logic.dart';

class AuthCarBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => AuthCarLogic());
  }
}
